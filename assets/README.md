## Introduction

This is the README for the TANGO source distribution release. TANGO is a
toolkit for building object oriented control systems based on CORBA and ZMQ.
TANGO is a joint effort of several major research institutes and industry
partners joined in the [tango controls](https://www.tango-controls.org)
organization.

See [here](lib/cpp/INSTALL.md) for the installation requirements for the C++
packages. For the java packages we require Java 1.8 or higher.

The default Pogo application in this package can generate source code
for C++, Python and Java device servers.

The database server delivered with this package do not force the database
user to be root. You have to use the my.cnf configuration files
or the MYSQL_USER and MYSQL_PASSWORD environment variables to specify
the database account to be used by the database server.

The TangoTest device server delivered with this package is an example
implementation of all possible Tango data types.
The displayed data is animated what creates some CPU load
when running the server. Starting an atkpanel (generic Tango client)
on a device of this server is CPU and memory consuming, because
all possible data are read and displayed.

### What's Inside

This source code release contains:

- Tango C++ library source files and java library and application jar files
- The Tango database device server source files.
- The Jive application.
- The Astor application and its associated Starter device server source files.
- The Pogo application.
- The tg_devtest application
- A test device server called TangoTest with its source files
- The atk graphical toolkit for writing tango applications in java
- The atkpanel application as a generic test client
- The atktuning application for tuning attributes in a device
- The logviewer application for visualising logging messages
- The atkmoni application to monitor scalar attribute values in time
- The jdraw synoptic editor to create synoptic applications
- The synopticAppli to run and test synoptics created with jdraw
- The Tango controlled access device server source file
  It is used if you want to run Tango with its controlled access feature
- A Tango Rest server implementation

The Jive application is a Tango database browsing tool written in Java. It also
allows device testing. Its documentation is located
[here](https://jive.readthedocs.io).

The Astor application is also a Java application. With the help of the Tango
starter device server, it allows full remote Tango control system
administration, see
[here](https://tango-controls.readthedocs.io/en/latest/tools-and-extensions/built-in/astor/index.html).

The Pogo application is a graphical Tango device server code generator, see
[here](https://tango-controls.github.io/pogo).

tg_devtest is a device testing application for device server.
It is a sub-part of Jive.

### Prerequisites

Before compiling and installing TANGO you need to install:

1. omniORB available from https://sourceforge.net/projects/omniorb (Preferably
   version 4.2.5 but older versions are fine as well).
   A patch is necessary for versions 4.2.0 and 4.2.1. Please refer to the
   [patches](https://tango-controls.readthedocs.io/en/latest/installation/patches.html#version-9-3-3-and-9-2-5-source-patches)
   section:

   To get the patch and informations on how to apply it. OmniORB is mandatory
   to build the Tango libraries and the delivered Tango device servers.

2. zmq available from https://www.zeromq.org (version >= 4.0.5).
	 Zmq is mandatory to build the Tango libraries and the delivered Tango
	 device servers.

3. cppzmq available from https://github.com/zeromq/cppzmq.

3. mysql available from http://mysql.org (version >= 5.5) or MariaDB from
   https://mariadb.org (version >= 10.0) One of these two databases is
   mandatory to install a Tango database.

4. doxygen available from https://www.doxygen.org

    Remark: doxygen is not mandatory to run Tango. It is just used by our code
             generator (called Pogo) to automatically generate some parts of
             the Tango device server documentation. If you don't install it,
             this part of the documentation will simply not be generated.
             But in all cases, some HTML pages documenting Tango device server
             will allways be generated.

5. libjpeg-turbo available from https://www.libjpeg-turbo.org/ (version >= 1.5.1).
   This is an optional dependency for cpptango which is requested by default.
   To build cpptango without it, use the cmake option `-DTANGO_USE_JPEG=OFF`.

Also check the tools you are using:

- cmake 3.7 or higher available from https://cmake.org
- C++14 capable compiler
- Java 1.8 or higher from https://www.oracle.com/java/technologies.
  Java is mandatory to install and run Tango tools like Pogo, Astor and Jive
  for instance.

### Installation

Once you have installed the packages in prerequisites you can think
about configuring and compiling TANGO.

To configure TANGO you need to tell it where to find omniORB, ZMQ and
mysql (if it is in a non-standard place) and where to install
TANGO.

The following table lists the cmake variables most commonly used for tweaking:

| Variable name                |  Default value                         | Description
|------------------------------|----------------------------------------|--------------------------------------------------------------------------------------------------
| `BUILD_SHARED_LIBS`          | `ON`                                   | Build cppTango as shared library, `OFF` creates a static library
| `CMAKE_BUILD_TYPE`           | `Release`                              | Compilation type, can be `Release`, `Debug` or `RelWithDebInfo/MinSizeRel` (Linux only)
| `CMAKE_INSTALL_PREFIX`       | `/usr/local` or `C:/Program Files`     | Desired install path
| `TANGO_CPPZMQ_BASE`          |                                        | cppzmq installed path
| `TANGO_IDL_BASE`             |                                        | tangoidl installed path
| `TANGO_JPEG_BASE`            |                                        | libjpeg installed path
| `TANGO_OMNI_BASE`            |                                        | omniORB4 installed path
| `TANGO_ZMQ_BASE`             |                                        | libzmq installed path
| `TANGO_USE_JPEG`             | `ON`                                   | Build with jpeg support, in this case a jpeg library implementation is needed.
| `TDB_DATABASE_SCHEMA`        | `OFF`                                  | Create the tango database schema
| `TSD_DATABASE`               | `ON`                                   | Compilation and installation of database server
| `TSD_JAVA_PATH`              | (empty)                                | java installed path
| `TSD_JAVA`                   | `ON`                                   | Installation of java applications
| `TSD_RC_FILE`                | `/etc/tangorc`                         | Location of the tango config file
| `TSD_TAC`                    | `OFF`                                  | Compilation and installation of access control server
| `MYSQL_HOST`                 | `localhost`                            | Host of the database server
| `MYSQL_ADMIN`                | (empty)                                | Name of the database user
| `MYSQL_ADMIN_PASSWD`         | (empty)                                | Password for the database user
| `TANGO_DB_NAME`              | `tango`                                | Name of the database

At the end of the cmake configuration step a status is displayed indicating pathes
and versions of found software and the parts of the Tango package which are
ready to be installed.

```
 Source code location: /home/firma/devel/TangoSourceDistribution/build/tango-dev
 Version: 9.4.0-rc2-14-gdbd0c73
 Compiler: C GNU 10.2.1, C++ GNU 10.2.1

 OMNIORB PATH: /usr
 OMNIORB VERSION: 4.2.2

 ZMQ PATH: /usr
 ZMQ VERSION: 4.3.4

 JAVA PATH: /usr/bin/java
 JAVA VERSION: 1.8.0_333

 Database:
 CLIENT LIB: /usr/lib/x86_64-linux-gnu/libmariadbclient.so
 CLIENT VERSION: 3.2.6                                                                                
 SERVER VERSION: 10.6.7-MariaDB-2ubuntu1.1
 CONNECTION:

 build:

 libraries:              ON (hardcoded)
 java application:       ON
 access control server:  OFF
 database server:        ON
 database schema create: ON
```

### Java applications

Tango arrives with many Java applications. Each java application has
its own script (in ${CMAKE_INSTALL_PREFIX}/bin) where the CLASSPATH is set before
starting the java interpreter. These scripts will be modified by the
cmake scripts to set a correct path to the java interpreter found in
your PATH when cmake was run.

For use with HiDPI, also called Retina or 4k/5k/6k displays, the JAVA user
interfaces can be too small. One can override the window scaling by setting the
environment variable `GDK_SCALE` to an integer larger than 1, e.g. on Linux `export
GDK_SCALE=2`. See also https://developer.gnome.org/gtk3/stable/gtk-x11.html.

### doxygen

Doxygen is a documentation generator from C++ source files. It is used by the
Tango code generator (called Pogo) to generate Tango device server documentation.
The script used to start Pogo will be modified by cmake to set
a correct path to the doxygen command found in your PATH when cmake was
run. It is not mandatory to find the doxygen command. This will only prevent
some part of Tango device server documentation to be generated by Pogo.

### Systemd integration

For integrating tango with systemd see the
[documentation](https://tango-controls.readthedocs.io/en/latest/tutorials-and-howtos/how-tos/how-to-integrate-with-systemd.html).

### Compiling

Once cmake has run successfully you can compile and install the
executables, include files, libraries and scripts. Do this by typing:

```
cmake --build .
cmake --install .
```

### Running

To test wether the TANGO build worked do the following:

1. Start the database on port 10000 by typing:

  `$TANGO_INSTALL_DIR/bin/DataBaseds 2 -ORBendPoint giop:tcp::10000`

	 Specify the database account to be used by the database server.
	 Two possibilities are available:

    - set the following environment variables to the account and
		  password to be used

			MYSQL_USER to specify the database login name
			MYSQL_PASSWORD to specify the password
			MYSQL_HOST if required

    - configure the database client with the database configuration
		  file my.cnf

      ```
      [client]
        user      = [database login name]
        password  = [password]
      ```

2. Configure the `TANGO_HOST` environment variable with the hostname the
   database server is running on and the port number used.

   `setenv TANGO_HOST hostname:10000`

3. Start the test device server tangoTest:

   `$TANGO_INSTALL_DIR/bin/TangoTest test`

4. Start jive by typing for example:

  `$TANGO_INSTALL_DIR/bin/jive`

5. Test your device using the test device in jive

6. Write new device servers using pogo:

  `$TANGO_INSTALL_DIR/bin/pogo`

### Updating from previous tango releases

No database changes from 9.3.4 to 9.4.0.

#### Updating from a tango 9.2.5 or older database

To update your database, follow these instructions:

- Stop your control system and your database server(s)

- Backup your database (Recommended, not mandatory)

- Cd to the `$TANGO_INSTALL_DIR/share/tango/db` directory

- Run the appropriate update script:

`mysql -u[user] -p[password] < ./update_db_from_9.2.5_to_9.3.4.sql`

- Restart your database server(s)

Similar scripts exist for older tango versions.

### Documentation

The full documentation for Tango is available at https://tango-controls.readthedocs.io
If Tango is properly installed at your site, a copy of the documentation should be available in
`$TANGO_INSTALL_DIR/share/doc/tango`.

Sphinx is used to generate the tango documentation html pages. Nevertheless, if
you want to re-generate the documentation, you can use the command `make html`
in doc/src directory. This requires Sphinx and IPython to be installed on your
host.

On a Debian-like system, you can install these tools using the following commands:

```
  sudo apt install -y python python-pip
  pip install sphinx ipython
```

### Getting help

- Email: info@tango-controls.org
- Forum: https://www.tango-controls.org/community/forum
- Slack: https://tango-controls.slack.com/

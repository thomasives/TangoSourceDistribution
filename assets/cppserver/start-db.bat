@ECHO OFF

GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: start-db.bat
# Author:   N.leclercq
# Date:     04/25/2003
# Purpose:  Launch the TANGO database device server
#
# Modifed:  21/10/2010 by E.Taurel
#			Do not use the full TANGO_HOST to start the db server
#			Only use the port number. Allow localhost to be used in
#			TANGO_HOST
#
######################################################################
:SCRIPT_BEGIN

IF NOT DEFINED TANGO_ROOT (
 ECHO TANGO_ROOT is not defined. Aborting!
 ECHO Please define a TANGO_ROOT env. var. pointing to your TANGO install directory.
 PAUSE
 GOTO SCRIPT_END
)

CALL "%TANGO_ROOT%\bin\common.bat"

FOR /f "delims=: tokens=2" %%G in ("%TANGO_HOST%") DO databaseds 2 -ORBendPoint giop:tcp:0.0.0.0:%%G -poolSize 3

GOTO SCRIPT_END

:USAGE

   ECHO Usage: start-db.bat
   PAUSE

:SCRIPT_END

@ECHO OFF

GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: start-pogo.bat
# Author:   N.leclercq
# Date:     04/25/2003
# Purpose:  Launch pogo
#
######################################################################
:SCRIPT_BEGIN

IF NOT DEFINED TANGO_ROOT (
 ECHO TANGO_ROOT is not defined. Aborting!
 ECHO Please define a TANGO_ROOT env. var. pointing to your TANGO install directory.
 PAUSE
 GOTO SCRIPT_END
)

set TANGO_JAVA_ROOT=%TANGO_ROOT%\share\tango\java
set TMPLT="%TANGO_ROOT%\pogo\templates\pogo"
set SOURCE_PATH=%TANGO_ROOT%\cppserver

set CLASSPATH=%TANGO_ROOT%\pogo\preferences
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\Pogo-9.6.31.jar

start javaw -DTEMPL_HOME=%TMPLT% -Dfile.encoding=ISO-8859-1 org.tango.pogo.pogo_gui.Pogo

:SCRIPT_END

@ECHO OFF

GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: start-logviewer.bat
# Author:   N.leclercq
# Date:     04/25/2003
# Purpose:  Launch the LogViewer
#
######################################################################
:SCRIPT_BEGIN

IF NOT DEFINED TANGO_ROOT (
 ECHO TANGO_ROOT is not defined. Aborting!
 ECHO Please define a TANGO_ROOT env. var. pointing to your TANGO install directory.
 PAUSE
 GOTO SCRIPT_END
)

IF {%1} == {} (
 ECHO Starting LogViewer in dynamic mode.
) ELSE (
 ECHO Starting LogViewer in static mode.
)

CALL "%TANGO_ROOT%\bin\common.bat"

start javaw -DTANGO_HOST=%TANGO_HOST% fr.esrf.logviewer.Main %1

:SCRIPT_END


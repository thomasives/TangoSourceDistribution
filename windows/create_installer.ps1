<# Create the Windows installer from the TSD tarball.

The script requires the following executables to be found on the path:

    - cmake 3.22.3
    - 7z 22.0
    - innosetup 6.2.0
    - GNU wget 1.21.3

The script also expects the TSD tarball (e.g. tango-dev.tar.gz) to be located
in the .\build directory.
#>
[CmdletBinding(DefaultParameterSetName='By_FromStep')]
param(
[Parameter(ParameterSetName='List')]
[Switch]$List,
[Parameter(ParameterSetName='By_FromStep')]
[String]$FromStep = 'Clean',
[Parameter(ParameterSetName='By_FromStep')]
[String]$ToStep = 'RunIssScript',
[Parameter(Mandatory=$true,ParameterSetName='By_Step')]
[String]$Step
)

if ($ARCH -eq $null) {
    $ARCH =  "x64"
}
if ($MYSQL_VERSION -eq $null) {
    $MYSQL_VERSION =  "5.7"
}
if ($MYSQL_VERSION_PATCH -eq $null) {
    $MYSQL_VERSION_PATCH =  "36"
}
if ($CPPTANGO_MSVC_VERSION -eq $null) {
    $CPPTANGO_MSVC_VERSION =  "v141"
}
if ($GENERATOR_VERSION -eq $null) {
    $GENERATOR_VERSION =  "Visual Studio 17 2022"
}
if ($BUILD_CONFIGURATION -eq $null) {
    $BUILD_CONFIGURATION =  "Release"
}
if ($CPPTANGO_PROJECT_ID -eq $null) {
    $CPPTANGO_PROJECT_ID =  "tango-controls%2FcppTango"
}
if ($CI_API_V4_URL -eq $null) {
    $CI_API_V4_URL = "https://gitlab.com/api/v4"
}

if ($ARCH -ne "x64") {
    throw "Unsupport architecture '${ARCH}'"
}

$error.Clear()
$VS_YEAR = ($GENERATOR_VERSION | Select-String -Pattern "Visual Studio (\d{2}) (\d{4})").Matches.Groups[2].Value
# We install the redistributible package for these Visual Studio years.  See Invoke-CreateIssScriptStep below.
if ($error.Count -ne 0 -or ! @("2015", "2017", "2019", "2022").Contains($VS_YEAR)) {
    throw "Unsupported generator '${GENERATOR_VERSION}'"
}

$CPPTANGO_VERSION = (Select-String -Path .\distribution.properties -Pattern "^cppTango" `
                    | Select-Object -Expand Line -First 1).Split("=")[1]

$GIT_DIR = $PWD
$WS_DIR = "C:\"tango_windows_installer_workspace"
$DEPS_DIR = "${WS_DIR}\deps"
$SETUP_DIR = "${WS_DIR}\setup"

$LIBTANGO = "libtango_${CPPTANGO_VERSION}_${CPPTANGO_MSVC_VERSION}_${ARCH}"
$TANGO_PATH = "${DEPS_DIR}/${LIBTANGO}"
$MYSQL = "mysql-${MYSQL_VERSION}.${MYSQL_VERSION_PATCH}-winx64"
$MYSQL_PATH = "${DEPS_DIR}/${MYSQL}"

function Invoke-CleanStep {
    Remove-Item -Path ${WS_DIR} -Recurse -Force -ErrorAction Ignore
}

function Invoke-FetchDepsStep {
    Remove-Item -Path ${DEPS_DIR} -Recurse -Force -ErrorAction Ignore

    New-Item ${DEPS_DIR} -ItemType Directory

    $CPPTANGO_RELEASE_URL = "${CI_API_V4_URL}/projects/${CPPTANGO_PROJECT_ID}/releases"
    $RELEASE = Invoke-WebRequest -UseBasicParsing "$CPPTANGO_RELEASE_URL" `
        | Select-Object -Expand Content | ConvertFrom-Json `
        | Where-Object { $_.tag_name -eq $CPPTANGO_VERSION }
    if ($RELEASE -eq $null) {
        throw "Could not find cppTango release corresponding to tag $CPPTANGO_VERSION"
    }
    $ZIPFILES = @("${LIBTANGO}_shared_release.zip")
                  # "${LIBTANGO}_shared_debug.zip",
                  # "${LIBTANGO}_static_release.zip",
                  # "${LIBTANGO}_static_debug.zip")
    [array]$ASSETS = $RELEASE.assets.links | Where-Object { $_.name -in $ZIPFILES }

    if ($ZIPFILES.Length -ne $ASSETS.Length) {
        Write-Output $ZIPFILES
        Write-Output $ASSETS
        throw "Missing assets.  Found $($ASSETS.Length), expected $($ZIPFILES.Length)"
    }

    foreach($asset in $ASSETS){
        Write-Output $asset
        $name = $asset.name
        wget.exe -q $asset.url -O ${DEPS_DIR}/$name
        if ($LASTEXITCODE -ne 0) { throw "wget.exe failed with exit code $LASTEXITCODE" }
        7z x "${DEPS_DIR}/$name" -o"${DEPS_DIR}"
        if ($LASTEXITCODE -ne 0) { throw "7z failed with exit code $LASTEXITCODE" }
    }

    # mysql
    wget.exe -q "https://dev.mysql.com/get/Downloads/MySQL-${MYSQL_VERSION}/${MYSQL}.zip" -P "${DEPS_DIR}"
    if ($LASTEXITCODE -ne 0) { throw "wget.exe failed with exit code $LASTEXITCODE" }
    7z x "${DEPS_DIR}/${MYSQL}.zip" -o"${DEPS_DIR}"
    if ($LASTEXITCODE -ne 0) { throw "7z failed with exit code $LASTEXITCODE" }

    7z x "build/tango-*.tar.gz" -o"${DEPS_DIR}"
    if ($LASTEXITCODE -ne 0) { throw "7z failed with exit code $LASTEXITCODE" }
    7z x "${DEPS_DIR}/tango-*.tar" -o"${DEPS_DIR}"
    if ($LASTEXITCODE -ne 0) { throw "7z failed with exit code $LASTEXITCODE" }
}

function Invoke-PrepareSetupStep {
    Remove-Item -Path ${SETUP_DIR} -Recurse -Force -ErrorAction Ignore

    New-Item ${SETUP_DIR} -ItemType Directory

    New-Item ${SETUP_DIR}/bin -ItemType Directory
    New-Item ${SETUP_DIR}/cppserver -ItemType Directory
    New-Item ${SETUP_DIR}/doc -ItemType Directory
    New-Item ${SETUP_DIR}/include -ItemType Directory
    New-Item ${SETUP_DIR}/lib -ItemType Directory
    New-Item ${SETUP_DIR}/share -ItemType Directory
    New-Item ${SETUP_DIR}/share/tango -ItemType Directory
    New-Item ${SETUP_DIR}/share/tango/java -ItemType Directory
    New-Item ${SETUP_DIR}/share/tango/idl -ItemType Directory
    New-Item ${SETUP_DIR}/share/tango/db -ItemType Directory

    Copy-Item -Path "${DEPS_DIR}/${LIBTANGO}_shared_release/*" -Destination "${SETUP_DIR}" -Recurse -Force

    Copy-Item -Path "${MYSQL_PATH}/lib/libmysql.dll" -Destination "${SETUP_DIR}/bin" -Force

    $TSD_PATH = Resolve-Path ${DEPS_DIR}\tango-*\ | Select-String -Pattern ".*tar$" -NotMatch

    Copy-Item -Path "$TSD_PATH/cppserver/tangotest" -Destination "${SETUP_DIR}/cppserver/" -Recurse -Force
    $UNEEDED = @("${SETUP_DIR}/cppserver/tangotest/doc_html",
                  "${SETUP_DIR}/cppserver/tangotest/Doxyfile",
                  "${SETUP_DIR}/cppserver/tangotest/pom.xml",
                  "${SETUP_DIR}/cppserver/tangotest/.gitlab-ci.yml",
                  "${SETUP_DIR}/cppserver/tangotest/.mailmap")

    foreach($file in $UNEEDED) {
        If (Test-Path -Path $file) {
            Remove-Item -Recurse $file
        }
    }

    Copy-Item "${GIT_DIR}/windows/README.txt"  -Destination "${SETUP_DIR}"
    $TSD_VERSION =  (Select-String -Path ${DEPS_DIR}\tango-*\CMakeLists.txt -Pattern "^Version: (\S*).*").Matches.Groups[1].Value
    Write-Output "TangoSourceDistribution=${TSD_VERSION}" > ${SETUP_DIR}/versions.txt
    Select-String -Path .\distribution.properties -Pattern "^#", "-repo=" -NotMatch | Select-Object -Expand Line >> ${SETUP_DIR}/versions.txt
}

function Invoke-BuildAndInstallTSDStep {
    $TSD_PATH = Resolve-Path ${DEPS_DIR}\tango-*\ | Select-String -Pattern ".*tar$" -NotMatch

    if (Test-Path -Path $TSD_PATH\build\install_manifest.txt) {
        Get-Content $TSD_PATH\build\install_manifest.txt | Remove-Item -ErrorAction Ignore
    }
    Remove-Item ${TSD_PATH}\build -Recurse -Force -ErrorAction Ignore

    # MySQL_VERSION set to avoid a call to try_run to find mysql version
    # CMAKE_CXX_FLAGS/CMAKE_EXE_LINKER_FLAGS set to generate pdb files for cppservers
    cmake -B"${TSD_PATH}\build" -S"${TSD_PATH}" -G"${GENERATOR_VERSION}" -A"${ARCH}" `
      -DTSD_LIB=OFF -DTSD_TAC=ON `
      -DCMAKE_CXX_FLAGS="/Zi" `
      -DCMAKE_EXE_LINKER_FLAGS="/debug" `
      -DCMAKE_INSTALL_PREFIX="${SETUP_DIR}" -DMySQL_ROOT="${DEPS_DIR}/${MYSQL}" `
      -DMySQL_VERSION="${MYSQL_VERSION}.${MYSQL_VERSION_PATCH}"
    if ($LASTEXITCODE -ne 0) { throw "cmake failed with exit code $LASTEXITCODE" }
    cmake --build ${TSD_PATH}\build --config "${BUILD_CONFIGURATION}"
    if ($LASTEXITCODE -ne 0) { throw "cmake failed with exit code $LASTEXITCODE" }
    cmake --install ${TSD_PATH}\build --config "${BUILD_CONFIGURATION}"
    if ($LASTEXITCODE -ne 0) { throw "cmake failed with exit code $LASTEXITCODE" }

    Remove-Item -Path ${SETUP_DIR}/lib/pkgconfig -Recurse
    Copy-Item -Path ${TSD_PATH}\build\cppserver\database\Release\Databaseds.pdb ${SETUP_DIR}\bin
    Copy-Item -Path ${TSD_PATH}\build\cppserver\starter\Release\Starter.pdb ${SETUP_DIR}\bin
    Copy-Item -Path ${TSD_PATH}\build\cppserver\tangoaccesscontrol\TangoAccessControl\Release\TangoAccessControl.pdb ${SETUP_DIR}\bin
    Copy-Item -Path ${TSD_PATH}\build\cppserver\tangotest\Release\TangoTest.pdb ${SETUP_DIR}\bin
}

function Get-InstallerVersion {
    $TSD_VERSION = (Select-String -Path ${SETUP_DIR}/versions.txt -Pattern "^TangoSourceDistribution" `
                        | Select-Object -Expand Line -First 1).Split("=")[1]
    return "${TSD_VERSION}_${CPPTANGO_MSVC_VERSION}"
}

function Get-InstallerName {
    $VERSION = Get-InstallerVersion
    return "tango-${VERSION}_win64"
}

function Invoke-CreateIssScriptStep {
    $VERSION = Get-InstallerVersion

    # Download Visual Studio 2013 Redistributable Package Installer for mysql.dll
    wget.exe -q http://aka.ms/highdpimfc2013x64enu -O"${WS_DIR}/vcredist2013_x64.exe"
    if ($LASTEXITCODE -ne 0) { throw "wget.exe failed with exit code $LASTEXITCODE" }

    # Download Visual Studio 2015-2022 Redistributable Package Installer for cppservers
    wget.exe -q https://aka.ms/vs/17/release/vc_redist.x64.exe  -O"${WS_DIR}/vcredist2022_x64.exe"
    if ($LASTEXITCODE -ne 0) { throw "wget.exe failed with exit code $LASTEXITCODE" }

    &"${GIT_DIR}\windows\make_iss_win.ps1" -Out "${WS_DIR}/tango.iss" -SetupDir "${SETUP_DIR}" -AppVersion "${VERSION}"
}

function Invoke-RunIssScriptStep {
    $NAME = Get-InstallerName
    iscc.exe /O"${WS_DIR}" /F"${NAME}" "${WS_DIR}\tango.iss"
    if ($LASTEXITCODE -ne 0) { throw "iscc.exe failed with exit code $LASTEXITCODE" }
}

function Invoke-GetArtifactsStep {
    $NAME = Get-InstallerName
    $ret = @{}
    $ret.add("setupDir", "${SETUP_DIR}")
    $ret.add("issScript", "${WS_DIR}\tango.iss")
    $ret.add("installer", "${WS_DIR}\${NAME}.exe")

    return $ret
}

enum Step {
    Clean
    FetchDeps
    PrepareSetup
    BuildAndInstallTSD
    CreateIssScript
    RunIssScript
    GetArtifacts
}

$ErrorActionPreference = 'Stop'

if ($PSCmdlet.ParameterSetname -eq 'List') {
    [Enum]::GetNames("Step")
} elseif ($PSCmdlet.ParameterSetname -eq 'By_FromStep') {
    $FromStepEnum = [Step]$FromStep
    $ToStepEnum = [Step]$ToStep
    if ($FromStepEnum -le [Step]::Clean -and $ToStepEnum -ge [Step]::Clean) {
        Invoke-CleanStep
    }
    if ($FromStepEnum -le [Step]::FetchDeps -and $ToStepEnum -ge [Step]::FetchDeps) {
        Invoke-FetchDepsStep
    }
    if ($FromStepEnum -le [Step]::PrepareSetup -and $ToStepEnum -ge [Step]::PrepareSetup) {
        Invoke-PrepareSetupStep
    }
    if ($FromStepEnum -le [Step]::BuildAndInstallTSD -and $ToStepEnum -ge [Step]::BuildAndInstallTSD) {
        Invoke-BuildAndInstallTSDStep
    }
    if ($FromStepEnum -le [Step]::CreateIssScript -and $ToStepEnum -ge [Step]::CreateIssScript) {
        Invoke-CreateIssScriptStep
    }
    if ($FromStepEnum -le [Step]::RunIssScript -and $ToStepEnum -ge [Step]::RunIssScript) {
        Invoke-RunIssScriptStep
    }
    if ($FromStepEnum -le [Step]::GetArtifacts -and $ToStepEnum -ge [Step]::GetArtifacts) {
        Invoke-GetArtifactsStep
    }
} elseif ($PSCmdlet.ParameterSetname -eq 'By_Step') {
    switch ([Step]$Step) {
        Clean { Invoke-CleanStep }
        FetchDeps { Invoke-FetchDepsStep }
        PrepareSetup { Invoke-PrepareSetupStep }
        BuildAndInstallTSD { Invoke-BuildAndInstallTSDStep }
        CreateIssScript { Invoke-CreateIssScriptStep }
        RunIssScript { Invoke-RunIssScriptStep }
        GetArtifacts { Invoke-GetArtifactsStep }
    }
}
